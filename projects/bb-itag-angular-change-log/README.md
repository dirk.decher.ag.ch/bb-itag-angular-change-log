# bb-itag-angular-change-log library

This is an Angular-footer-change-log library, which gives you a ready-made change-log component. It is possible to customize the change-log with a config file.
This ReadMe explains how to integrate the change-log and how to configure it.

### Dependencies

The change-log library requires Angular Material, FlexLayout and FontAwesome.

These can all be integrated via the terminal:
* Angular Material: 
```
ng add @angular/material
```
* FontAwesome: 
```
npm i -s @fortawesome/fontawesome-svg-core @fortawesome/free-solid-svg-icons @fortawesome/angular-fontawesome@0.5.x
```

* FlexLayout: 
``` 
npm i -s @angular/flex-layout @angular/cdk
```

#### Installation

install the library via NPM:
```text
npm i -s bb-itag-angular-change-log
```

shortcut with all dependencies together:
```
npm i -s @fortawesome/fontawesome-svg-core @fortawesome/free-solid-svg-icons @fortawesome/angular-fontawesome@0.5.x @angular/flex-layout @angular/cdk bb-itag-angular-change-log
```

#### Using the library in the application

To use the library in your application, add to the app.module ts the following imports: 
``` typescript
import { BbItagAngularChangeLogModule, } from 'bb-itag-angular-change-log';
import { HttpClientModule } from '@angular/common/http';


// add the change-log module in the module imports
@NgModule({
  imports:
    BbItagAngularChangeLogModule,  
    HttpClientModule
})

```

add the change-log to your application. We recommend to create your own mainApp component but you can use it direct in the app-component files.

###### app.component.html

You now have to set the change-log in the template

``` html
<lib-bb-itag-angular-change-log></lib-bb-itag-angular-change-log>
```

But nothings displayed yet because you don't have any data to show.


#### Change-logs 

To display the change-logs you need a method called loadChange(). In this method you can decide from where the information
should get retrieved to be displayed as change-log. In the example it comes from a json file in the asset directory.

###### component.ts
``` typescript
import { Component } from '@angular/core';
import { IChangeLogInfo } from '../../projects/bb-itag-angular-change-log/src/lib/models';
import { Observable } from 'rxjs';
import { HttpBackend, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public changeLogInfos: IChangeLogInfo[];
  private httpClient: HttpClient;

  constructor(
    handler: HttpBackend
  ) {
    this.httpClient = new HttpClient(handler);
    this.loadChangeLog()
      .subscribe((value) => {
        this.changeLogInfos = value;
      });
  }
  public loadChangeLog(): Observable<IChangeLogInfo[]> {
    return this.httpClient.get<IChangeLogInfo[]>('/assets/config/changeLog.json');
  }

}
```

###### component.html

Then we refer in component.html changeLogInfos. 
``` html
 <lib-bb-itag-angular-change-log [changeLogInfos]="changeLogInfos"></lib-bb-itag-angular-change-log>
```
This is how it looks when you display the change-logs:
![change-logs](https://gitlab.com/bb-itag/bb-itag-angular-change-log/raw/master/projects/bb-itag-angular-change-log/src/images/change-log_default.PNG "change-log_default")

#### Change-Log file structure
In the example above a json file provides the data to show. The structure of the file should look like this to get displayed correctly:

```json
[
  {
    "version": "0.0.2",
    "releaseDate": "2019-11-13",
    "fixes": [
      {
        "issueNo": 1,
        "title": "first issue",
        "description": "description",
        "type": "bug"
      },
      {
        "issueNo": 2,
        "title": "second issue",
        "description": "description",
        "type": "feature"
      }
    ]
  }
]
```

Now the change-logs should be displayed. 

#### Class Input

The library allows you set an css class to change the look of the change log bar.
Add a reference to the template tag of the library:

``` html
<lib-bb-itag-angular-change-log classNameTitle="changeLogBar" ></lib-bb-itag-angular-change-log>
```

We called the class as example "changeLogBar". Now in the styles.css (it just needs to be global) create a css class and you can adapt the bar.
Then it could look like this example:
![change-log class](https://gitlab.com/bb-itag/bb-itag-angular-change-log/raw/master/projects/bb-itag-angular-change-log/src/images/change-log-class.PNG "change-log class")
