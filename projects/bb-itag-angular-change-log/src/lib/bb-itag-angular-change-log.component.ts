import { Component, Input } from '@angular/core';
import { IChangeLogInfo } from './models';

@Component({
  selector: 'lib-bb-itag-angular-change-log',
  templateUrl: 'bb-itag-angular-change-log.component.html',
  styleUrls: [ 'bb-itag-angular-change-log.component.css' ],
})
export class BbItagAngularChangeLogComponent {

  @Input()
  public changeLogInfos: IChangeLogInfo[];

  @Input()
  public classNameTitle = 'title';

}
