import { NgModule } from '@angular/core';
import { BbItagAngularChangeLogComponent } from './bb-itag-angular-change-log.component';
import { ChangeLogFixComponent } from './change-log-fix/change-log-fix.component';
import { MatButtonModule, MatCardModule, MatExpansionModule, MatTabsModule, MatToolbarModule } from '@angular/material';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CommonModule } from '@angular/common';
import { FlexModule } from '@angular/flex-layout';


@NgModule({
  declarations: [BbItagAngularChangeLogComponent, ChangeLogFixComponent],
  imports: [
    MatCardModule,
    FontAwesomeModule,
    MatExpansionModule,
    CommonModule,
    FlexModule,
    MatCardModule,
    MatTabsModule,
    MatToolbarModule,
    FontAwesomeModule,
    MatExpansionModule,
    MatButtonModule,
  ],
  exports: [BbItagAngularChangeLogComponent]
})
export class BbItagAngularChangeLogModule { }
