import { Component, Input, OnInit } from '@angular/core';
import { IChangeLogFixInfo } from '../models';
import { faBug, faPuzzlePiece } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'lib-change-log-fix',
  templateUrl: './change-log-fix.component.html',
  styleUrls: ['./change-log-fix.component.css']
})
export class ChangeLogFixComponent implements OnInit{

  typeIcon;

  @Input()
  public changeLogFix: IChangeLogFixInfo;

  ngOnInit() {
    if (this.changeLogFix.type === 'bug') {
      this.typeIcon = faBug;
    } else if (this.changeLogFix.type === 'feature') {
      this.typeIcon = faPuzzlePiece;
    }
  }


}
