import { Component } from '@angular/core';
import { IChangeLogInfo } from '../../projects/bb-itag-angular-change-log/src/lib/models';
import {faStickyNote} from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';
import { HttpBackend, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public faStickyNote = faStickyNote;

  public changeLogInfos: IChangeLogInfo[];
  private httpClient: HttpClient;

  constructor(
    handler: HttpBackend
  ) {
    this.httpClient = new HttpClient(handler);
    this.loadChangeLog()
      .subscribe((value) => {
        this.changeLogInfos = value;
      });
  }
  public loadChangeLog(): Observable<IChangeLogInfo[]> {
    return this.httpClient.get<IChangeLogInfo[]>('/assets/config/changeLog.json');
  }

}
