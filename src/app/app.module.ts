import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BbItagAngularChangeLogModule } from '../../projects/bb-itag-angular-change-log/src/lib/bb-itag-angular-change-log.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule, MatCardModule, MatExpansionModule, MatTabsModule, MatToolbarModule } from '@angular/material';
import { FlexModule } from '@angular/flex-layout';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BbItagAngularChangeLogModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    HttpClientModule,
    MatCardModule,
    MatTabsModule,
    MatToolbarModule,
    FontAwesomeModule,
    FlexModule,
    MatExpansionModule,
    MatButtonModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
