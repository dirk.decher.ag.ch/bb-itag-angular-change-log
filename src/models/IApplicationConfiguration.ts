export interface IApplicationConfiguration {
  production: boolean;
  showDebug: boolean;
}
